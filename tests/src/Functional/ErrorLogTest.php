<?php

namespace Drupal\Tests\error_log\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Error Log module.
 *
 * @group error_log
 */
class ErrorLogTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to install.
   *
   * @var string[]
   */
  protected static $modules = ['error_log'];

  /**
   * Tests Error Log module.
   */
  public function testErrorLog(): void {
    $admin_user = $this->drupalCreateUser(['administer site configuration']);
    $this->assertInstanceOf(AccountInterface::class, $admin_user);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/development/logging');
    $this->submitForm([], 'Save configuration');
    $log = file(DRUPAL_ROOT . '/' . $this->siteDirectory . '/error.log');
    $this->assertIsArray($log);
    $this->assertCount(3, $log);
    $this->assertSame(1, preg_match('/\[.*\] \[[a-z]+\] \[user\] .* Session opened for /', $log[1]));
  }

}
