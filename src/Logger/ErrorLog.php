<?php

namespace Drupal\error_log\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Logger\LogMessageParserInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Logger\RfcLoggerTrait;
use Psr\Log\LogLevel;
use Psr\Log\LoggerInterface;

/**
 * Logs events to the PHP error log.
 */
class ErrorLog implements LoggerInterface {

  use DependencySerializationTrait;
  use RfcLoggerTrait;

  /**
   * Provides untranslated log levels.
   */
  const LOG_LEVELS = [
    RfcLogLevel::EMERGENCY => LogLevel::EMERGENCY,
    RfcLogLevel::ALERT => LogLevel::ALERT,
    RfcLogLevel::CRITICAL => LogLevel::CRITICAL,
    RfcLogLevel::ERROR => LogLevel::ERROR,
    RfcLogLevel::WARNING => LogLevel::WARNING,
    RfcLogLevel::NOTICE => LogLevel::NOTICE,
    RfcLogLevel::INFO => LogLevel::INFO,
    RfcLogLevel::DEBUG => LogLevel::DEBUG,
  ];

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The message's placeholders parser.
   *
   * @var \Drupal\Core\Logger\LogMessageParserInterface
   */
  protected $parser;

  /**
   * Constructs an Error Log object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory object.
   * @param \Drupal\Core\Logger\LogMessageParserInterface $parser
   *   The parser to use when extracting message variables.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LogMessageParserInterface $parser) {
    $this->configFactory = $config_factory;
    $this->parser = $parser;
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore missingType.parameter
   */
  public function log($level, $message, array $context = []): void {
    global $base_url;
    $config = $this->configFactory->get('error_log.settings');
    $log_levels = $config->get('log_levels');
    if (!is_array($log_levels) || !is_scalar($level) || empty($log_levels["level_$level"])) {
      return;
    }
    $ignored_channels = $config->get('ignored_channels');
    if (is_array($ignored_channels) && in_array($context['channel'], $ignored_channels)) {
      return;
    }
    // Drush handles error logging for us, so disable redundant logging.
    if (function_exists('drush_main') && !ini_get('error_log')) {
      return;
    }
    $message_placeholders = $this->parser->parseMessagePlaceholders($message, $context);
    $format = $config->get('format');
    if (!is_string($format)) {
      $format = '[!level] [!type] [!ip] [uid:!uid] [!request_uri] [!referer] !message';
    }
    error_log(strtr($format, [
      '!base_url' => $base_url,
      '!ip' => $context['ip'],
      '!level' => static::LOG_LEVELS[$level],
      '!link' => $context['link'],
      '!message' => empty($message_placeholders) ? $message : strtr($message, $message_placeholders),
      '!request_uri' => $context['request_uri'],
      '!referer' => $context['referer'],
      '!severity' => $level,
      '!timestamp' => $context['timestamp'],
      '!type' => $context['channel'],
      '!uid' => $context['uid'],
    ]));
  }

}
