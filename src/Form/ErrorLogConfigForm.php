<?php

namespace Drupal\error_log\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;

/**
 * Implements an Error Log config form.
 */
class ErrorLogConfigForm {

  /**
   * Builds Error Log config form.
   *
   * @param mixed[] $form
   *   The logging and errors config form.
   */
  public static function buildForm(array &$form): void {
    $config = \Drupal::configFactory()->getEditable('error_log.settings');
    $form['error_log'] = [
      '#type'          => 'details',
      '#title'         => t('Error Log'),
      '#tree'          => TRUE,
      '#open'          => TRUE,
      '#description'   => error_log_help('help.page.error_log', \Drupal::routeMatch()),
    ];
    $options = [];
    foreach (RfcLogLevel::getLevels() as $key => $value) {
      $options["level_$key"] = $value;
    }
    $default_value = [];
    $log_levels = $config->get('log_levels');
    if (is_array($log_levels)) {
      foreach ($log_levels as $key => $value) {
        $default_value[$key] = $value ? $key : 0;
      }
    }
    $form['error_log']['log_levels'] = [
      '#type'          => 'checkboxes',
      '#title'         => t('Log levels'),
      '#description'   => t('Check the log levels which should be sent to the PHP error log.'),
      '#options'       => $options,
      '#default_value' => $default_value,
    ];
    $ignored_channels = $config->get('ignored_channels');
    if (!is_array($ignored_channels)) {
      $ignored_channels = [];
    }
    $form['error_log']['ignored_channels'] = [
      '#type'          => 'textarea',
      '#title'         => t('Ignored channels'),
      '#description'   => t('A list of log channels for which messages should not be sent to the PHP error log (one channel per line). Commonly-configured log channels include <em>access denied</em> for 403 errors and <em>page not found</em> for 404 errors.'),
      '#default_value' => implode("\n", $ignored_channels),
    ];
    $variables = ['#type' => 'html_tag', '#tag' => 'dl'];
    foreach ([
      '!base_url' => t('Base URL of the site.'),
      '!ip' => t('IP address of the user triggering the message.'),
      '!level' => t('The severity level of the event as an untranslated string; ranges from emergency to debug.'),
      '!link' => t('A link to associate with the message.'),
      '!message' => t('The message to store in the log.'),
      '!referer' => t('HTTP Referer if available.'),
      '!request_uri' => t('The requested URI.'),
      '!severity' => t('The severity level of the event as an integer; ranges from 0 (Emergency) to 7 (Debug).'),
      '!timestamp' => t('Unix timestamp of the log entry.'),
      '!type' => t('The category to which this message belongs.'),
      '!uid' => t('User ID.'),
    ] as $variable => $description) {
      $variables['child'][] = [
        '#type' => 'html_tag',
        '#tag' => 'dt',
        'child' => [
          '#type' => 'html_tag',
          '#tag' => 'code',
          '#value' => $variable,
        ],
      ];
      $variables['child'][] = [
        '#type' => 'html_tag',
        '#tag' => 'dd',
        '#value' => $description,
      ];
    }
    $form['error_log']['format'] = [
      '#type' => 'textfield',
      '#title' => t('Log format'),
      '#default_value' => $config->get('format') ?? '[!level] [!type] [!ip] [uid:!uid] [!request_uri] [!referer] !message',
      '#description' => t('Specify the format of the log entry. Available variables are: @variables', ['@variables' => \Drupal::service('renderer')->render($variables)]),
      '#maxlength' => 280,
    ];
    $form['#submit'][] = [self::class, 'submitForm'];
  }

  /**
   * Submits Error Log config form.
   *
   * @param mixed[] $form
   *   The logging and errors config form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function submitForm(array &$form, FormStateInterface $form_state): void {
    $ignored_channels = $form_state->getValue([
      'error_log',
      'ignored_channels',
    ]);
    if (!is_string($ignored_channels)) {
      $ignored_channels = '';
    }
    \Drupal::configFactory()->getEditable('error_log.settings')
      ->set('log_levels', $form_state->getValue(['error_log', 'log_levels']))
      ->set('ignored_channels', static::extractIgnoredChannels($ignored_channels))
      ->set('format', $form_state->getValue(['error_log', 'format']))
      ->save();
  }

  /**
   * Extracts list of ignored channels from string.
   *
   * @return string[]
   *   Array of ignored channels.
   */
  public static function extractIgnoredChannels(string $string): array {
    $ignored_channels = preg_split('/\R/', $string, -1, PREG_SPLIT_NO_EMPTY);
    return is_array($ignored_channels) ? array_map('trim', $ignored_channels) : [];
  }

}
