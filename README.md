# Error Log

Once Drupal bootstraps, it sends only a subset of errors to PHP's error log —
which could be, for example, an error log in Apache, or stderr on the command
line.

Error Log module adds the PHP error log as a Drupal logger implementation, so
errors will once again appear in the same log or console that they appeared in
before Drupal bootstrapped.

Error Log module can be configured at the logging and errors configuration page
(admin/config/development/logging).  You can toggle each log level from
emergency to debug, configure channels to ignore (e.g. `page not found`), and
specify the log format.

Please file bug reports and feature requests at the [Error Log project
page](https://www.drupal.org/project/error_log).

Error Log module is maintained by [mfb](https://www.drupal.org/u/mfb).
